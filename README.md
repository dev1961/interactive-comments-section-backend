# Contribute

## Requirements
- Node 16+
- Yarn

## Installation

- `yarn`

## Running local

- `yarn dev`

## Adding new packages

- `yarn add ~package-name~`

## Running local in production mode

- `yarn build`
- `yarn start`

