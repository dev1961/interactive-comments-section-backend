FROM node:16-alpine3.14
ENV NODE_ENV=production
WORKDIR /app
COPY ["package.json", "yarn.lock", "./"]
RUN yarn --production
COPY . .
RUN yarn build
EXPOSE 8000
CMD [ "yarn", "start" ]